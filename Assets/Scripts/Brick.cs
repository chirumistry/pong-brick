﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public float hits = 1;
    public float points = 100;
    public Vector3 rotator;
    public Material hitMatirial;
    public AudioSource hitBrickAudio;
    Material _orgMatirial;
    Renderer _renderer;
    void Start()
    {
        transform.Rotate(rotator * (transform.position.x - transform.position.y) * 0.2f);
        _renderer = GetComponent<Renderer>();
        hitBrickAudio = GetComponent<AudioSource>();
        _orgMatirial = _renderer.sharedMaterial;
    }

    void Update()
    {
        transform.Rotate(rotator * Time.deltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        hitBrickAudio.Play();
        --hits;
        if (hits <= 0)
        {
            GameManager.Instatnce.Score += points;
            Invoke("DestroyObject", 0.05f);
        }
        _renderer.sharedMaterial = hitMatirial;
        Invoke("RestoreMatirial", 0.05f);
    }

    void RestoreMatirial()
    {
        _renderer.sharedMaterial = _orgMatirial;
    }

    void DestroyObject()
    {
        Destroy(gameObject);
    }
}
