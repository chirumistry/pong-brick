﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicControl : MonoBehaviour
{
    public AudioMixer musicMixer;

    public void setMusicLavel(float sliderVol)
    {
        musicMixer.SetFloat("MusicVol", Mathf.Log10(sliderVol) * 20);
    }

    public void setSFXLavel(float sliderVol)
    {
        musicMixer.SetFloat("SFXVol", Mathf.Log10(sliderVol) * 20);
    }
}
