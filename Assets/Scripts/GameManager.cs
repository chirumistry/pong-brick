﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject ballPrefab;
    public GameObject playerPrefab;

    public Text scoreText;
    public Text ballText;
    public Text levelText;
    public Text highScroreText;
    public Text currentScoreText;

    public GameObject pannelMenu;
    public GameObject panelPlay;
    public GameObject panelLevelComplete;
    public GameObject panelGameOver;
    public GameObject panelGamePause;
    public GameObject pannelSetting;

    public GameObject[] levels;

    public static GameManager Instatnce { get; private set; }

    public enum State { MENU, INIT, PLAY, LEVELCOMPLETED, LOADLEVEL, PAUSEGAME, SETTING, GAMEOVER }

    bool _switchingState;

    private bool _settingStartWithMenu;

    State _state;

    private float _score;

    public float Score
    {
        get { return _score; }
        set {
            _score = value;
            scoreText.text = "SCORE: " + _score;
            currentScoreText.text = $"CURRENT SCORE: {_score}";
        }
    }

    private int _ball;

    public int Ball
    {
        get { return _ball; }
        set { _ball = value;
            ballText.text = "BALLS: " + _ball;
        }
    }

    private int _level;

    public int Level
    {
        get { return _level; }
        set
        {
            _level = value;
            levelText.text = $"LEVEL: {_level}";
        }
    }

    GameObject _currentBall;
    GameObject _currentLevel;

    public void ClickPlay()
    {
        SwitchState(State.INIT);
    }

    public void ClickPauseGame ()
    {
        if (_settingStartWithMenu)
        {
            SwitchState(State.SETTING);
        }
        else
        {
            SwitchState(State.PAUSEGAME);
        }
    }

    public void ClickSettingButton()
    {
        SwitchState(State.SETTING);
    }

    public void ClickResume()
    {
        Cursor.visible = false;
        SwitchState(State.PLAY);
    }

    public void ClickBackButton()
    {
        if (_settingStartWithMenu)
        {
            SwitchState(State.MENU);
        } else
        {
            SwitchState(State.PAUSEGAME);
        }
    }

    public void ClickExit()
    {
        Application.Quit();
    }

    void Start()
    {
        Instatnce = this;
        SwitchState(State.MENU);
    }

    public void SwitchState(State newState, float delay = 0)
    {
        StartCoroutine(SwitchDelay(newState, delay));
    }

    IEnumerator SwitchDelay(State newState, float delay = 0)
    {
       _switchingState = true;
        yield return new WaitForSeconds(delay);
        EndState();
        _state = newState;
        BeginState(newState);
        _switchingState = false;

    }

    void BeginState(State newState)
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        switch (newState)
        {
            case State.MENU:
                Cursor.visible = true;
                _settingStartWithMenu = true;
                highScroreText.text = $"HIGHSCORE: {PlayerPrefs.GetFloat("highscore")}";
                pannelMenu.SetActive(true);
                break;
            case State.INIT:
                _settingStartWithMenu = false;
                Cursor.visible = false;
                panelPlay.SetActive(true);
                Score = 0;
                Level = 0;
                Ball = 3;
                if (_currentBall != null)
                {
                    Destroy(_currentLevel);
                }
                Instantiate(playerPrefab);
                SwitchState(State.LOADLEVEL);
                break;
            case State.PLAY:
                Time.timeScale = 1;
                break;
            case State.PAUSEGAME:
                Cursor.visible = true;
                Time.timeScale = 0;
                panelGamePause.SetActive(true);
                break;
            case State.SETTING:
                pannelSetting.SetActive(true);
                break;
            case State.LOADLEVEL:
                if (Level >= levels.Length)
                {
                    SwitchState(State.GAMEOVER);
                } else
                {
                    _currentLevel = Instantiate(levels[Level]);
                    SwitchState(State.PLAY);
                }

                break;
            case State.LEVELCOMPLETED:
                Destroy(_currentLevel);
                Destroy(_currentBall);
                panelLevelComplete.SetActive(true);
                break;
            case State.GAMEOVER:
                panelGameOver.SetActive(true);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (_state)
        {
            case State.MENU:
                Destroy(_currentLevel);
                break;
            case State.INIT:
                break;
            case State.PLAY:
                if (_currentBall == null)
                {
                    if (Ball > 0)
                    {
                        _currentBall = Instantiate(ballPrefab);
                    }
                    else
                    {
                        SwitchState(State.GAMEOVER);
                    }
                }
                if (_currentLevel != null && _currentLevel.transform.childCount == 0 && !_switchingState)
                {
                    Destroy(_currentBall);
                    Destroy(_currentLevel);
                    Level++;
                    SwitchState(State.LEVELCOMPLETED);
                }
                if (Input.GetKey(KeyCode.Escape))
                {
                    ClickPauseGame();
                }
                break;
            case State.SETTING:

                break;
            case State.LOADLEVEL:
                break;
            case State.LEVELCOMPLETED:
                if (Input.anyKeyDown)
                {
                    SwitchState(State.LOADLEVEL, 0.5f);
                }
                break;
            case State.GAMEOVER:
                if (Score >= PlayerPrefs.GetFloat("highscore"))
                {
                    PlayerPrefs.SetFloat("highscore", Score);
                }
                if (Input.anyKeyDown)
                {
                    SwitchState(State.MENU);
                }
                break;
        }
    }

    void EndState()
    {
        switch (_state)
        {
            case State.MENU:
                pannelMenu.SetActive(false);
                break;
            case State.INIT:
                break;
            case State.PLAY:
                break;
            case State.PAUSEGAME:
                panelGamePause.SetActive(false);
                break;
            case State.SETTING:
                pannelSetting.SetActive(false);
                break;
            case State.LOADLEVEL:
                break;
            case State.LEVELCOMPLETED:
                panelLevelComplete.SetActive(false);
                break;
            case State.GAMEOVER:
                panelGameOver.SetActive(false);
                break;
        }
    }
}
